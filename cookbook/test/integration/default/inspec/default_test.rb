# # encoding: utf-8

# Inspec test for recipe codius-hosting::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

# Test the hostname.
describe sys_info do
    its('hostname') { should eq 'dev.codius.chainsmarts.io' }
end

# This package list is the specification for the to the attribute default['codius']['packages']['dependencies']
packages = [
    'epel-release',
    'gcc-c++',
    'git',
    'make',
    'nginx',
    'nodejs'
]
packages.each do |p|
    describe package p do
        it { should be_installed }
    end    
end

describe service 'hyperd' do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
end 

describe file('/tmp/hyperd-bootstrap.sh') do 
    it { should exist }
end

describe file('/tmp/nodejs-setup_10.x.sh') do
    it { should exist }
end

describe file('/tmp/moneyd-xrp-4.0.0-1.x86_64.rpm') do
    it { should exist }
end

describe package('moneyd-xrp') do
    it { should be_installed }
end

describe service 'moneyd-xrp' do 
    it { should be_installed }
    it { should be_enabled }
    it { should_not be_running }
end

describe file '/etc/systemd/system/codiusd.service' do
    it { should exist }
end

describe service 'codiusd' do
    it { should be_installed }
    it { should_not be_enabled }
    it { should_not be_running }
end

# TODO: Test for the absense of gcc-c++ make when the build is completd in prod 
# (developer tools should not be on a production system).
# Test for presense of deps if dev.

# Test Hyperd.

# Test Moneyd.

# Test Codiusd.