# Opscode that sets up your Codius Hosting Service:

This repository was inspired [this article written by Stefan Thomas](https://medium.com/codius/how-to-run-your-own-codius-host-42e13afe1fb2).

The purpose of this code base is to provide a simple and consistent method for deploying a "developer ready" or "production ready" Codius Hosting Service.

## Quick start (for someone wanting to deploy a Codius Hosting Service):

For the current release of this project you have to be familiar with AWS and Chef.
The fastest way to get setup is to setup the following items.
1. VPC
1. Subnet
1. Security Group
1. Internet Gateway
1. Route Table

When those are setup in AWS then use the cookbook with Opsworks to provision a host.

## In the future the Quickstart will be to just use the app.

Run the ChainSmartsManager.

What exactly does the app do?
1. It checks to be sure you have everything needed for the script to run (jq, aws-cli, etc...).
1. It prompts you to setup external dependencies (XRP testnet account, domain name).
1. It gathers information from you and stores it on your local computer at ~/.codius-developer-profile.
1. It sets up all the infrastructure (OpsWorks, VPC, Security Groups, EC2 instance, IAM Roles, load balancers, etc...). Currently AWS is the only cloud provider supported (if you want others just [submit a feature request](https://gitlab.com/jfgrissom/opscode-codius-hosting/issues)).
1. Then it uses information gathered from you and the information provided by AWS about the deployed infrastructure and completes the setup of your Codius Hosting Service (SSL certificates, DNS names, configuration files, etc...).
1. It sets up everything using reasonable security standards (enabling the firewall on the host, configuring security groups to only allow ssh access from your ip, setting up a VPC with an ingress subnet that only exposes access to https, etc...).
1. And... most importantly, it kicks off tests everything so you know it's all setup correctly.

## If you wish to contribute to this code base you'll need these.

A contributor markdown is coming. For now these can get you started.

1. Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads).
1. Download and install [Vagrant](https://www.vagrantup.com/downloads.html).
1. Download and install the [Chef Development Kit](https://downloads.chef.io/chefdk).
