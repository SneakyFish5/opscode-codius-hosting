AWSTemplateFormatVersion: "2010-09-09"
Description: Provides infrastructure for Codius Hosting Services

Parameters:
  # This as the metadata used to create the CIDR block of the VPC.
  VpcSupernet:
    Type: String
    Default: 192.168.32.0/24
  ProjectName:
    Type: String
    Default: codius-service
  # Override this value with the environment you are creating (prod, int, dev, etc...).
  Environment:
    Type: String
    Default: dev
  # This has to be passed in.
  ManagementHostIp:
    Type: String

Resources:
# Define the virtual private cloud.
  CodiusServiceVpc: 
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VpcSupernet
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags: 
        - Key: Name 
          Value: !Sub ${Environment}-${ProjectName}-vpc
        - Key: Project
          Value: !Sub ${Environment}-${ProjectName}

# Define a subnet that is meant to allow internet traffic into the VPC.
  IngressSubnet: 
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref CodiusServiceVpc
      # This assigns this to the first AZ. If the community wants to create some fault tolerance 
      # (which increases the initial investment) we can add other AZs and auto-scaling groups.
      AvailabilityZone: !Select [ 0, !GetAZs '' ]
      CidrBlock: !Ref VpcSupernet
      # By default we don't want the hosts in this subnet to come up with a public IP.
      # To prevent the IP addresses from changing we'll assign an elastic IP address.
      MapPublicIpOnLaunch: false
      Tags: 
        - Key: Name 
          Value: !Sub ${Environment}-${ProjectName}-ingress-subnet
        - Key: Project
          Value: !Sub ${Environment}-${ProjectName}

# Define the gateway to the internet.
  InternetGateway:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: Name
          Value: !Sub ${Environment}-${ProjectName}-igw
        - Key: Project
          Value: !Sub ${Environment}-${ProjectName}
  InternetGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref CodiusServiceVpc

# Define routes for the vpc.
  PublicRouteTable:
    Type: "AWS::EC2::RouteTable"
    Properties: 
      VpcId: !Ref CodiusServiceVpc
      Tags:
        - Key: Name 
          Value: !Sub ${Environment}-${ProjectName}-public-rtb
        - Key: Project
          Value: !Sub ${Environment}-${ProjectName}
  DefaultPublicRoute: 
      Type: AWS::EC2::Route
      DependsOn: InternetGatewayAttachment
      Properties: 
          RouteTableId: !Ref PublicRouteTable
          DestinationCidrBlock: 0.0.0.0/0
          GatewayId: !Ref InternetGateway

# Define a security group that EC2 instances can use.
  CodiusSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties: 
      VpcId: !Ref CodiusServiceVpc
      GroupName: !Sub ${Environment}-${ProjectName}-sg
      GroupDescription: Provides access from a single IP for ssh and opens up https to the world. 
      SecurityGroupIngress:
        - CidrIp: 0.0.0.0/0
          IpProtocol: tcp
          FromPort: 443
          ToPort: 443
        - CidrIp: !Sub ${ManagementHostIp}/32
          IpProtocol: tcp
          FromPort: 22
          ToPort: 22
      Tags: 
        - Key: Name
          Value: !Sub ${Environment}-${ProjectName}-sg
        - Key: Project
          Value: !Sub ${Environment}-${ProjectName}

# Bind the route table to the ingress subnet.
  BindToRouteTable:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties: 
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref IngressSubnet