# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

- TBD: (ChainSmartsManager) User interface.
- TBD: (ChainSmartsManager) creates DNS record.
- TBD: (ChainSmartsManager) connects to the host and creates SSL certificates runs certbot.
- TBD: (ChainSmartsManager) recommends post installation steps (including starting moneyd-xrp with owners private xrp key).
- TBD: (ChainSmartsManager) runs integration test to confirm everything is setup correctly.
- TBD: (Cloudformation) sets up opsworks stack.


## [v0.2.0] - 2018-6-20

### Added
- (Cloudformation) constructs a virtual private cloud.
- (Cloudformation) constructs a subnet for ingress (basically a DMZ).
- (Cloudformation) constructs and internet gateway for the vpc.
- (Cloudformation) constructs route table.
- (Cloudformation) constructs security group that limits ingress.
- (Scripts) Deploys codius-infrastructure.yaml (scripts/deploy_codius_infrastructure.sh)
- (Scripts) Evaluates user's public IP and defines that as the managementIP for the Cloudformation Security Group.
- (Cookbook) added kitchen.aws.yaml to reflect the correct infastructure provider for the test kithcen.
- (Cookbook) created .kitchen.yml as a symlink to make it easy to switch between infrastructure providers for testing.

### Changed
- (Cookbook) modified the readme to be sure test kitchen leverages the infrastructure provided by the (Cloudformation) template.
- (Cookbook) changed kitchen.local.yml to kitchen.virtualbox.yaml to reflect the infrastructure provider.

### Security
- Made sure local environment variable file (~/.codius-service-profile) is used for anything that might be sensitive.

## [v0.1.0] - 2018-6-16

### Added
- Setup (Cookbook) to easily deploy Codius.
- Setup .kitchen.local.yml to run integration tests.
- Support for stand alone codius host deployment.
- (Cookbook) supports CentOS7
- (Cookbook) sets up the hostname based on an attribute that can be overridden.
- (Cookbook) sets up the domain name based on an attribute that can be overridden.
- (Cookbook) leverages public script to compile and installs hyperd service.
- (Cookbook) endables hyperd service.
- (Cookbook) leverages public script to compile nodejs 10.4.x from source.
- (Cookbook) installs nodejs.
- (Cookbook) installs moneyd-xrp service.
- (Cookbook) intentionally doesn't start the moneyd-xrp service (because it requires an XRP private key so instructions to do this manually are provided as a post installation step).
- (Cookbook) leverages npm module to install codiusd.
- (Cookbook) installs codiusd as a service.
- (Cookbook) intentionally disables codiusd as a service (it relies on moneyd so is handles in a post provisioning step).