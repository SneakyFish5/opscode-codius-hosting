#!/usr/bin/env bash

source ~/.codius-service-profile

# Get the public IP address of the gateway this host uses.
MANAGEMENT_IP=$(curl 'https://api.ipify.org?format=json' | jq -r ".ip")

aws cloudformation create-stack \
  --stack-name "${ENVIRONMENT}-codius-infrastructure" \
  --region "us-east-1" \
  --template-body "file://cloudformation/codius-infrastructure.yaml" \
  --parameters \
    ParameterKey=ManagementHostIp,ParameterValue=${MANAGEMENT_IP} \
    ParameterKey=Environment,ParameterValue=${ENVIRONMENT}